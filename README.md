# m1-dotnet-container-x64-lab

ASP.NET Core/.NET Core support on m1 with container x64

## Getting started

Failed : Original Vesrion of .NET Core/.NET 6 template.
Success : Force Turn off impact from the line of code manually.

```shell
# build image
docker build -t 'lab-m1' --build-arg entrypoint=WebApplication1 .

# Run 
docker run --rm -it -p 5001:80 "lab-m1"
```
